package com.spring.test.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.test.dto.ResponseDto;
import com.spring.test.model.User;
import com.spring.test.service.AuthenticateService;
import com.spring.test.util.PasswordEncode;
import com.spring.test.util.ResponseCodes;
/*
 * Controller to receive request and send response
 */
@RestController
@CrossOrigin
public class UserController {
	
	@Autowired
	AuthenticateService authenticateService;
	
	/*
	 *  Endpoint to save recieved request in database
	 */
	@PostMapping(value="saveUser")
	public ResponseEntity<?> saveUser(@RequestBody User user){
		ResponseDto response = new ResponseDto();
		try {
			// Calling method to encrpt password 
			String encryptedPassword = PasswordEncode.hashStringPassword(user.getPassword());

			User userModel = new User(user.getFirstName(), user.getLastName(), user.getEmailId(),user.getUserName(),
					 encryptedPassword);
			
			// Calling service layer
			authenticateService.saveUser(userModel);

			response.setMessage("User saved successfully.");
			response.setStatus(ResponseCodes.SUCCESS);
			response.setTimestamp(new Date().toString());
		} catch (Exception e) {
			response.setMessage("User failed to save.");
			response.setStatus(ResponseCodes.ERROR);
			response.setTimestamp(new Date().toString());
			response.setError(e);
		}
		return ResponseEntity.ok(response);		
	}



	/*
	 *  Endpoint to validate user 
	 */
	@PostMapping(value = "/authenticate")
	public ResponseEntity<?> authenticateUser(@RequestBody User user ) {
		ResponseDto response=new ResponseDto();
		try {
			User userCredentials = authenticateService.getUserByUserName(user.getUserName()).get(0);
		if ((user.getUserName().equals(userCredentials.getUserName())) && (PasswordEncode.hashStringPassword(user.getPassword()).equals(userCredentials.getPassword()))){
				
				response.setMessage("User authenticated successfully.");
				response.setStatus(ResponseCodes.SUCCESS);
				response.setTimestamp(new Date().toString());
				userCredentials.setPassword(null); // Set password NULL
				response.setResponse(userCredentials);
				return ResponseEntity.ok(response);
			}
			
			response.setMessage("User authentication failed.");
			response.setStatus(ResponseCodes.ERROR);
			response.setTimestamp(new Date().toString());

		} catch (Exception e) {
			e.printStackTrace();
			
			response.setMessage("User authentication failed.");
			response.setStatus(ResponseCodes.ERROR);
			response.setTimestamp(new Date().toString());
			response.setError(e);
		}

		return ResponseEntity.ok(response);
		
		
	}
	

	/*
	 *  Endpoint to get list of users 
	 */
	@GetMapping(value = "/userslist")
	public ResponseEntity<?> getUsersList() {
		ResponseDto response = new ResponseDto();
		try {
			List<User> userList = authenticateService.getAllUsersList();
			response.setMessage("All users list.");
			response.setStatus(ResponseCodes.SUCCESS);
			response.setTimestamp(new Date().toString());
			response.setResponse(userList);
		} catch (Exception e) {
		
			response.setMessage("Could not fetch users list.");
			response.setStatus(ResponseCodes.ERROR);
			response.setTimestamp(new Date().toString());
			response.setError(e);
		}

		return ResponseEntity.ok(response);
	}

}
