package com.spring.test.dao;

import java.io.Serializable;
import java.util.List;

import com.spring.test.model.User;
/*
 * Interface ued to declare methods
 */
public interface UserDao {

	List<User> getUserByUserName(String userName);

	Serializable saveUser(User userModel);

	List<User> getAllUsersList();

}
