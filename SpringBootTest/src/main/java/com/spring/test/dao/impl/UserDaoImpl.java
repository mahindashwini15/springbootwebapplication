package com.spring.test.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.test.dao.UserDao;
import com.spring.test.model.User;
/*
 * Dao Layer Class
 */
@Repository
public class UserDaoImpl implements UserDao {

	// Created instance of SessionFactory
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	// Method to get lists of users by userName
	@Override
	public List<User> getUserByUserName(String userName) {
		List<User> usersList = null;
		try {
			Session session = this.sessionFactory.getCurrentSession();
			usersList = session.createQuery("from User WHERE userName='" + userName + "'").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return usersList;
	}

	//Method to save user in database
	@Override
	public Serializable saveUser(User userModel) {
		Session session = this.sessionFactory.getCurrentSession();
		return session.save(userModel);
	}

	//Method to get list of users
	@Override
	public List<User> getAllUsersList() {
		List<User> usersList = null;
		try {
			Session session = this.sessionFactory.getCurrentSession();
			usersList = session.createQuery("from User").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return usersList;
	}

}
