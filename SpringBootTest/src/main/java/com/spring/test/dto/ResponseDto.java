package com.spring.test.dto;

import java.io.Serializable;
/*
 * ResponseDto pojo class to return response
 */
public class ResponseDto implements Serializable {
	private String timestamp;
	private int status;
	private Exception error;
	private String message;
	private Object response;

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Exception getError() {
		return error;
	}

	public void setError(Exception error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "ResponseDto [timestamp=" + timestamp + ", status=" + status + ", error=" + error + ", message="
				+ message + ", response=" + response + "]";
	}

}
