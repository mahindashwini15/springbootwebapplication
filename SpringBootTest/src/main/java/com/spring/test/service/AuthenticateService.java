package com.spring.test.service;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.test.dao.UserDao;
import com.spring.test.model.User;
/*
 * Service layer class for user
 */
@Service("AuthenticateService")
public class AuthenticateService {
		
	//Creating instance of UserDao class
	@Autowired
	UserDao userDao;
	
	@Transactional
	public List<User> getUserByUserName(String userName) {
		//Calling Dao layer
		return userDao.getUserByUserName(userName);
	}

	@Transactional
	public Serializable saveUser(User userModel) {
		//Calling Dao layer
		return userDao.saveUser(userModel);		
	}

	@Transactional
	public List<User> getAllUsersList() {
		//Calling Dao layer
		return userDao.getAllUsersList();
	}
	
	
	

}
