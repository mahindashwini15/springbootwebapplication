package com.spring.test.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/*
 * Class to encrypt String
 */
public class PasswordEncode {

	public static String hashStringPassword(String password) {

		StringBuilder hash = new StringBuilder();
		try {
			MessageDigest sha = MessageDigest.getInstance("SHA-256");
			byte[] hashedBytes = sha.digest(String.valueOf(password).getBytes());
			char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
					'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

			for (int idx = 0; idx < hashedBytes.length; ++idx) {
				byte b = hashedBytes[idx];
				hash.append(digits[(b & 0xf0) >> 4]);
				hash.append(digits[b & 0x0f]);
			}

		} catch (NoSuchAlgorithmException exception) {

		}

		return hash.toString();
	}

}
