package com.spring.test.util;
/*
 * Responses code to send in response
 */
public class ResponseCodes {
	public static int SUCCESS = 200;
	public static int ERROR = 400;
}
